/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.shapeinheritance;

/**
 *
 * @author bwstx
 */
public class Rectangle extends Shape {
   protected double width;
   protected double height;
    
    
    public Rectangle(double width, double height){
        super();
        System.out.println("Rectangle Created");
        this.width = width;
        this.height = height;
    }
    
    
    @Override
    public double calArea(){
         return width*height;
    }
    
}
