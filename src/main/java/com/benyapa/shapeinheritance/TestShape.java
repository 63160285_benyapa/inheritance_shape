/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.shapeinheritance;

/**
 *
 * @author bwstx
 */
public class TestShape {
    public static void main(String[] args) {
        //Circle
        Circle circle1 = new Circle(3);
        System.out.println("Circle 1");
        System.out.println(circle1.calArea());
        circle1.printLine();
        Circle circle2 = new Circle(4);
        System.out.println("Circle 2");
        System.out.println(circle2.calArea());
        circle2.printLine();
        
        //Triangle
        Triangle tri = new Triangle(4,3);
        System.out.println("Triangle");
        System.out.println(tri.calArea());
        tri.printLine();
        
        //Rectangle
        Rectangle rec = new Rectangle(3,4);
        System.out.println("Rectangle");
        System.out.println(rec.calArea());
        rec.printLine();
        
        //Square
        Square square = new Square(2,2);
        System.out.println("Square");
        System.out.println(square.calArea());
        square.printLine();
        
        //checkCircle1
        System.out.println("Circle1 is Circle :" + (circle1 instanceof Circle));
        System.out.println("Circle1 is Rectangle :" + (circle1 instanceof Object));
        System.out.println("Circle1 is Square :" + (circle1 instanceof Object));
        System.out.println("Circle1 is Triangle :" + (circle1 instanceof Object));
        System.out.println("Circle1 is Shape :" + (circle1 instanceof Shape));
        System.out.println();
         //checkCircle2
        System.out.println("Circle2 is Circle :" + (circle2 instanceof Circle));
        System.out.println("Circle2 is Rectangle :" + (circle2 instanceof Object));
        System.out.println("Circle2 is Square :" + (circle2 instanceof Object));
        System.out.println("Circle2 is Triangle :" + (circle2 instanceof Object));
        System.out.println("Circle2 is Shape :" + (circle2 instanceof Shape));
        System.out.println();
        //checkTriangle
        System.out.println("Triangle is Circle :" + ( tri instanceof Object));
        System.out.println("Triangle is Rectangle :" + ( tri instanceof Object));
        System.out.println("Triangle is Square :" + ( tri instanceof Object));
        System.out.println("Triangle is Triangle :" + ( tri instanceof Triangle));
        System.out.println("Triangle is Shape :" + ( tri instanceof Shape));
        System.out.println();
        //checkRectangle
        System.out.println("Rectangle is Circle :" + ( rec instanceof Object));
        System.out.println("Rectangle is Rectangle :" + ( rec instanceof Rectangle));
        System.out.println("Rectangle is Square :" + ( rec instanceof Object));
        System.out.println("Rectangle is Triangle :" + ( rec instanceof Object));
        System.out.println("Rectangle is Shape :" + ( rec instanceof Shape));
        System.out.println();
        //checkSquare
        System.out.println("Square is Circle :" + ( square instanceof Object));
        System.out.println("Square is Rectangle :" + ( square instanceof Rectangle));
        System.out.println("Square is Square :" + ( square instanceof Square));
        System.out.println("Square is Triangle :" + ( square instanceof Object));
        System.out.println("Square is Shape :" + ( square instanceof Shape));
        System.out.println();
        
        
        Shape[] shapes = {circle1,circle2,tri,rec,square};
        for (int i = 0; i <shapes.length; i++) {
            if(shapes[i] instanceof Circle){
                Circle cir = (Circle)shapes[i];
                System.out.println("Circle :"+cir.calArea());
            }
             if(shapes[i] instanceof Triangle){
                Triangle triangle = (Triangle)shapes[i];
                System.out.println("Triangle :"+tri.calArea());
            }
              if(shapes[i] instanceof Rectangle){
                Rectangle rectangle = (Rectangle)shapes[i];
                System.out.println("Rectangle :"+rectangle.calArea());
            }
              if(shapes[i] instanceof Square){
                Square squ = (Square)shapes[i];
                System.out.println("Square :"+square.calArea());
            }
              
             
            
        }
        
    }
}
