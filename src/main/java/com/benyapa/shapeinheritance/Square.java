/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.shapeinheritance;

/**
 *
 * @author bwstx
 */
public class Square extends Rectangle{
    public Square(double width,double height){
        super(2,2);
        System.out.println("Square created");
    }
    
    @Override
    public double calArea(){
        return width*height;
    }
}
